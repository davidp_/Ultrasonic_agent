package paquete;
import java.util.Random;

import lejos.nxt.*;
import lejos.util.*;
import lejos.robotics.*;
import lejos.robotics.navigation.DifferentialPilot;

public class Agente {
	
	private static int velRobot = 6;
	private static int velGiro = 50;
	private static float diaLlanta = 2.1f;
	private static float disLlantas = 4.4f;
	private static float distanciaObjeto = 0;
	private static double angulo = 10.0;
	private static boolean estado = false;

	public static void main(String[] args) {
		UltrasonicSensor sensor = new UltrasonicSensor(SensorPort.S4);
		DifferentialPilot piloto = new DifferentialPilot(diaLlanta, disLlantas, Motor.A, Motor.C, true);
		
		try {
			Thread.sleep(3000);
		}catch(Exception e) {}
		piloto.setTravelSpeed(velRobot);
		do {
			piloto.setTravelSpeed(velRobot);
			sensor.continuous();
			distanciaObjeto = sensor.getDistance();
			LCD.clear();
			LCD.drawString("distancia: "+distanciaObjeto, 2, 3);
			if(distanciaObjeto < 50) {			
				detener(piloto);
				evadir(piloto);
				angulo = angulo * 2;
			}else {
				avanzar(piloto);
				angulo = 10.0;
			}
		}while(!Button.ESCAPE.isDown());
	}

	public static void avanzar(DifferentialPilot piloto) {
		//LCD.clear();
		//LCD.drawString("Avanzando", 2, 3);
			piloto.forward();
	}
	public static void avanzar2(DifferentialPilot piloto) {
		reversa(piloto);
		piloto.forward();
	}
	public static void detener(DifferentialPilot piloto) {
		//LCD.clear();
		//LCD.drawString("En alto", 2, 3);
		piloto.stop();
	}
	
	public static void reversa(DifferentialPilot piloto) {
		piloto.backward();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
	}
	
	public static void evadir(DifferentialPilot piloto) {
		piloto.setRotateSpeed(velGiro);
		if(estado) {
			piloto.rotate(angulo);
		}else {
			piloto.rotate(angulo*-1);
		}
		estado = !estado;
	}
}
